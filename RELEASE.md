# Release Notes
## v0.4.4

And a big thank you to everyone using the extension. Your feedback, ideas and bug reports are making this tool better every day.

This is a small bug fix release.

This release removes the keepWorkFile setting. This was causing too many problems and was not worth trying to keep at this time. Perhaps we will bring it back again some other day. That is, if @Lubber lets me. :)

For a full list of changes, please see the projects [History](HISTORY.md) file.

Much more to come, check the roadmap, and let us know if there is something else you would like to be added by sending us a note on Facebook or creating an [Issue](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues) on the project page.

Cheers!
